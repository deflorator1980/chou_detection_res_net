#!/usr/bin/env python
# coding: utf-8

# In[10]:


from imageai.Detection import ObjectDetection
import os


# In[11]:


img = 'tsar.jpg'
# img = 'crow.png'
# img = 'chou.jpg'
# img = 'chou2.jpeg'
# img_out = 'chou_out_test.jpg'
# img = 'pig.jpg'
# img = 'adversarial.png'
# img = 'crow.png'
img_out = 'tmp.jpg'


# In[12]:


detector = ObjectDetection()

detector.setModelTypeAsRetinaNet()
detector.setModelPath("resnet50_coco_best_v2.0.1.h5")

# detector.setModelTypeAsYOLOv3()
# detector.setModelPath("yolo.h5")

detector.loadModel()
detections = detector.detectObjectsFromImage(input_image=img, output_image_path=img_out)

print(detections)
# for eachObject in detections:
#     print(eachObject["name"] + " : " + eachObject["percentage_probability"] )


# In[ ]:




